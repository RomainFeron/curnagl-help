# Conda on curnagl

## Installation

Because of permission issues, we need to each use our own conda installation.
I recommend installing conda somewhere in `/work/FAC/FBM/DEE/rwaterho/evofun/<your folder>`. Alternatively, you could install it in `/users/<your folder>` but you may reach the 100,000 files quota at some point.

Full instructions available [here](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html).

**Download the installer script and run it:**

```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Follow instructions from the prompt. 

**Restart your shell:**

```bash
source ~/.bashrc
```

**Note:** If you're not using bash as your shell, source the appropriate file (*e.g.* `~/.zshrc` if you're using zsh)

**Initialize conda for your shell:**

```bash
# Replace bash with whatever shell you are using
conda init bash
```

**Update conda:**

The Conda version from the official installer is not always the latest update. To make sure Conda is up-to-date, run:

```bash
conda update conda
```

## Cleanup

### Remove unused environments

Regularly list environments and remove unused ones.
In particular, snakemake creates a new environment every time you update an environment's yaml file,
so you can safely delete old environment folders.

```bash
# List all existing conda environments
conda env list

# Remove all packages in a specific environment
conda env remove -n <environment name>
conda env remove -p <environment path>
```

### Clean unused files

The following command removes index cache, lock files, unused cache packages, and tarballs.

```bash
conda clean --all
```
