# Commands to check resource usage on Curnagl

## Check resources for our group

**Get CPU hours for each group member starting from a specific date (here 2020-01-01):**
```bash
sreport -n -P -t Hour cluster AccountUtilizationByUser Accounts=rwaterho_evofun format=account,used,login Start=2020-01-01
```

**Get storage and number of file quotas for our project and your own user directory:**
```bash
quotacheck
```

**Count files**
```bash
cd /work/FAC/FBM/DEE/rwaterho/evofun
# Count files for each folder in the current directory
./count_files.sh
# Count files in each directory for a specific folder
./count_files.sh <DIRECTORY_NAME>
```

**Get size of each directory**
```bash
# This takes very long and can run into permission issues
cd /work/FAC/FBM/DEE/rwaterho/evofun
du -d1 -h
```

## Check general resources and spy on other people

**Get current usage of each filesystem:**
```bash
df -h
```

Important filesystems for us:
```
work                         1000T  127T  874T  13% /work
scratch                       110T   23T   88T  21% /scratch
nasdcsr.unil.ch:/RECHERCHE    1.5P  990T  434T  70% /nas
```

Max jobs running / submitted per user (1 job = 1 cpu):

```bash
sacctmgr show qos format=name,priority,GrpCPUs,maxjobs,maxsubmit
```

**Get CPU usage for each group (cannot check storage anymore):**
```bash
# Script that produces a simple tabulated output, edit script for start date and output file
python3 /work/FAC/FBM/DEE/rwaterho/evofun/romain/meta/curnagl_police.py
```

**Check if people are using the front-end:**
```bash
# I have a script logging front-end usage every 15 minutes, check logs:
less /work/FAC/FBM/DEE/rwaterho/evofun/romain/meta/monitoring.tsv
```
