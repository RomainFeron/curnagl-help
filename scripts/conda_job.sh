#!/bin/bash -l

#SBATCH --mail-type ALL
#SBATCH --mail-user livio.ruzzante@unil.ch
#SBATCH --chdir ./
#SBATCH --job-name arthropod-orthofinder-diamond
#SBATCH --error=/scratch/axiom/FAC/FBM/DEE/rwaterho/evofun/livio/orthofinder-phylogeny/arthropod-orthofinder-diamond.err
#SBATCH --output=/scratch/axiom/FAC/FBM/DEE/rwaterho/evofun/livio/orthofinder-phylogeny/arthropod-orthofinder-diamond.out
#SBATCH --partition axiom
#SBATCH --time=9-00:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem 40G

# Example script from Livio to run orthofinder installed with conda

source ~/.bashrc
conda activate orthofinder
orthofinder -f gene_sequences_diamond -S diamond -t 32
