#!/bin/sh
#SBATCH -J arthropoda_busco
#SBATCH --cpus-per-task=1
#SBATCH --mem=3000
#SBATCH --time=2-23:59:00
#SBATCH --error=snakemake.log
#SBATCH --output=snakemake.log

# Example script to run a snakemake workflow. The main snakemake process will be submitted as a job by this script, and steps from the workflow will be submitted as separate jobs using the 'slurm' profile.

source ~/.bashrc
conda activate snakemake

snakemake --unlock -j1
snakemake --rerun-incomplete --use-conda --keep-going --profile slurm --configfile config/curnagl.yaml
