#!/bin/sh
#SBATCH -J bwa
#SBATCH --cpus-per-task=16
#SBATCH --mem=20000
#SBATCH --time=2-23:59:00
#SBATCH --error=bwa.log
#SBATCH --output=bwa.log

module load gcc/9.3.0
module load bwa/0.7.17
module load samtools/1.4

bwa mem -t 16 data/genome.fa data/reads_p1.fq.gz data/reads_p2.fq.gz | samtools sort -O CRAM -o aligned_reads.cram  
