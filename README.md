# Cluster Info

Guidelines and tips to make using the Curnagl a happy experience

## Content

- **[Connect to Curnagl and setup your environment](Setup.md)**
- **[Check resources usage and quotas](Resources.md)**
- **[Setup and maintain Conda](Conda.md)**
