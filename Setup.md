# Connect to Curnagl and setup your environment

**To login on Curnagl you need to be on a UNIL IP address, so if you're not in the office you'll have to use the VPN**.

## Connect to Curnagl

**Basic login:**

```bash
ssh <yourlogin>@curnagl.dcsr.unil.ch
```

**Setup login without password:**

```bash
# Check if you already generated an RSA key (on your own computer)
ls ~/.ssh

# If there is no files called "id_rsa" and "id_rsa.pub", generate them with
ssh-keygen -t rsa -b 4096 -C "<your email address>"

# Copy the rsa key to the cluster
ssh-copy-id -i ~/.ssh/id_rsa <your login>@curnagl.dcsr.unil.ch

# Login on curnagl (say 'yes'), this should be the last time you need to enter your password
ssh <yourlogin>@curnagl.dcsr.unil.ch
```

## Folder structure

To keep our folders clean and organize, we each have a folder named after us within `/work/FAC/FBM/DEE/rwaterho/evofun/`. Keep all your work within your folder. Because of how default permissions are setup, we cannot easily share data and software between users (by default group members do not have write permission on your files and folders), so keep your software in a sub-directory within your own directory.

## Setup environment

**Just a collection of tips to make your life easier**

- To automatically start in the `evofun` directory after login, edit the file `/users/<your login>/.bashrc` to add the line (at the end):

```bash
cd /work/FAC/FBM/DEE/rwaterho/evofun/<your name>
```
- You can create aliases to quickly navigate directories or to use as shortcuts for common commands. To create an alias, add a line `alias=command` to the file `/users/<your login>/.bashrc`. Examples of aliases I use on curnagl:

```bash
# Aliases
alias gs="git status"
alias ga="git add"
alias gc="git commit -m"
alias gu="git pull"
alias gp="git push"
alias py="python3"
alias va="source venv/bin/activate"
alias gtw="cd /work/FAC/FBM/DEE/rwaterho/evofun/romain"
alias gtn="cd /nas/FAC/FBM/DEE/rwaterho/evofun"
```

- I recommend using a modern shell instead of standard bash, modern shell implements several time-saving and convenience features. I'm using zsh with the [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh) configuration. You can also checkout **fish shell**.

## Mount remote filesystems locally

On linux, most file managers can mount filesystems via **sftp**. The exact procedure depends on your file manager (each desktop environment has a default). It's also possible to do on OSX but a bit more complicated. For reference, here's the procedure for Thunar (XFCE default file manager):

1. In the file manager address bar, type:
```bash
sftp://<your_login>@curnagl.dcsr.unil.ch/work/FAC/FBM/DEE/rwaterho/evofun/
```

![SFTP Step 1](img/sftp_1.png "SFTP Step 1")

2. On the first connection, a prompt ask for your password and how long to remember it (I put "forever"). You can then browse and edit files on curnagl just like your local computer:

![SFTP Step 2](img/sftp_2.png "SFTP Step 2")

