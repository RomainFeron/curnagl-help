# Using Curnagl

The DCSR wiki is a good resource to check official information on Curnagl: [WIKI](https://wiki.unil.ch/ci/books/service-de-calcul-haute-performance-%28hpc%29)

## File systems

There are three file systems that we use on Curnagl:
- `/work/.../rwaterho/evofun` : main filesystem where we store data we are currently working on and install software. This space is not backed up.
- `/nas/.../rwaterho/evofun/` : long-term storage filesystem where we store important data from completed analyses or primary data that we generated. This space is backed up and a bit costly so compress your data before moving it there and don't keep temporary / intermediate files. There are three folders in this space: **D1c** (1 copy, no backup, use for not super important things that you don't need on /work anymore but still want to keep), **D2c** (2 copies backup, use for compressed analyses results / primary data), and **LTS** (long term storage on physical tapes, discuss together before using it).
- `/scratch/<username>` : temporary filesystem that can be erased regularly but is fast without quotas. Ideally you should use this to run your analyses instead of /work if you can.

## Job submission

Curnagl uses the **slurm** scheduler to handle jobs ([slurm documentation](https://slurm.schedmd.com/), [relevant wiki page](https://wiki.unil.ch/ci/books/service-de-calcul-haute-performance-%28hpc%29/page/how-to-run-a-job-on-the-clusters)).

### Partitions

Partitions are like sub-clusters with specific settings. Curnagl provides three main partitions for different types of jobs:
- **cpu**: standard partition for most jobs using a submission script
- **gpu**: special partition for jobs using GPUs (graphic cards)
- **interactive**: special partition to run commands directly instead of using scripts

You will have to specify which partition to use depending on how you want to submit a job (explained in next section).

The **cpu** partition is actually two partitions, one with higher memory nodes. You can see the details of all partitions with the command:

```bash
sinfo --noconvert -eO "partitionname,nodes,cpus,memory,time,maxcpuspernode,groups,available,prioritytier"
```

Output:

```
PARTITION           NODES               CPUS                MEMORY              TIMELIMIT           MAX_CPUS_PER_NODE   GROUPS              AVAIL               PRIO_TIER           
interactive         3                   48                  515400              8:00:00             UNLIMITED           all                 up                  1                   
cpu                 50                  48                  515400              3-00:00:00          UNLIMITED           all                 up                  1                   
cpu                 12                  48                  1031670             3-00:00:00          UNLIMITED           all                 up                  1                   
gpu                 7                   48                  515400              3-00:00:00          UNLIMITED           all                 up                  1  
```

In this output, CPUS and MEMORY give the number of CPUs and memory for each node (*i.e.* compute machine). For most cases take this as the maximum to use for your own jobs.

### Running jobs on the cluster

#### Run a command directly

To submit a command directly, use `srun`; you have to use the `interactive` partition:

```bash
srun --partition=interactive [job settings] <command>
```

#### Open an interactive session

You should avoid using the login node to run commands, except for very small jobs like basic file operation (copy, remove...). To run commands on a compute node in an interactive session, just submit a `bash` command to the `interactive` partition with `srun`:

```bash
srun --partition=interactive [job settings] bash
```

If you are using another shell (*e.g.* zsh), use that instead:

```bash
srun --partition=interactive [job settings] zsh
```

#### Submit a bash script

Most of the time you'll want to submit a job script. These are bash script files with the following format:

```bash
#!/bin/bash

some_command --some-argument
```

To submit a job script called `example.sh` to the `cpu` partition, use the `sbatch` command:

```bash
sbatch --partition=cpu [job settings] example.sh
```

#### Job settings

The `srun` and `sbatch` commands share a lot of arguments to specify job settings. These arguments can be specified directly in the submission command for both srun and sbatch, like `--partition=xxx` in the examples. When submitting a job script with `sbatch`, you can also specify arguments within the script with the syntax:

```bash
#!/bin/bash
#SBATCH <short-argument> <argument value>
#SBATCH <long-argument>=<argument value>

some_command --some-argument
```

Here's a list of most commonly used arguments:

| header | header |
| ------ | ------ |
| --job-name | Name of the job, makes it easier to find in the queue |
| --partition | Partition to submit the job to, as explained above |
| --time | Maximum runtime for this job (has a pretty strong effect on the job's priority) |
| --cpus-per-task | Number of cores to allocate for this job (note: you still need to specify threads in your actual command)  |
| --mem | Maximum memory to allocate for this job |
| --error | Path to a file to redirect stderr for the job  |
| --output | Path to a file to redirect stdout for the job (can be the same as --error) |
| --mail-type | Can alert you by email after specific events if you want (NONE, BEGIN, END, FAIL, REQUEUE, ALL) |
| --mail-user | Your email if you want emails defined by --mail-type (can get a bit spammy) |

#### Job scripts

Examples of job scripts for common cases are provided in the `scripts` folder.

## Monitoring jobs

### Checking queue

You can list all jobs currently running on curnagl with `squeue`; to see only your jobs, use `squeue -u <username>`. The output gives you the job ID, partition, name, status (**R**unning, **P**ending, **C**ancelled), time since job started, number of nodes, and node on which the job is running + reason for pending. When you reached the maximum number of jobs you can run concurrently, jobs will be pending with reason `QOSMaxNodePerUserLimit`.

### Checking a job information

To get detailed information on a queued job (running or not), use this command (get `jobid` from squeue output):

```bash
scontrol show job <jobid>
```

The output from this command gives you information on resources, status, and paths related to your job, which can be useful to understand why a job failed.

## Limitations

To check the maximum number of jobs you can run / submit at the same time, use 

```bash
sacctmgr show qos format=name,priority,GrpCPUs,maxjobs,maxsubmit  
```

Currently, we can have 256 jobs running concurrently and 1000 jobs in queue. Here 1 job corresponds to 1 cpu, e.g. you can run 16 jobs using 16 cpus each or 32 jobs using 8 cpus each.
